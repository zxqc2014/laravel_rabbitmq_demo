<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/10/25
 * Time: 15:58
 */

return [
    //demo消息队列
    'demo' => [

        'host' => env('DEMO_MQ_HOST', '127.0.0.1'),

        'port' => env('DEMO_MQ_PORT', 5672),

        'user' => env('DEMO_MQ_USER', 'demo'),

        'pwd' => env('DEMO_MQ_PWD', '181219'),

        'vhost' => env('DEMO_MQ_VHOST', 'demo'),
    ],
];

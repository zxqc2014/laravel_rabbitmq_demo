
SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for mq_process_error_log
-- ----------------------------
CREATE TABLE `mq_process_error_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `msg_id` bigint(20) NOT NULL,
  `process_msg` varchar(255) NOT NULL DEFAULT '' COMMENT '执行返回信息',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `idx_msg_id` (`msg_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='消息处理失败日志表';

-- ----------------------------
-- Table structure for mq_process_log
-- ----------------------------
CREATE TABLE `mq_process_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `msg_str` varchar(200) NOT NULL DEFAULT '' COMMENT '消息请求内容 json字符串',
  `msg_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '消息类型',
  `find_keyword` varchar(32) NOT NULL DEFAULT '' COMMENT '查找消息内容的关键字',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `process_num` tinyint(2) NOT NULL DEFAULT '0' COMMENT '执行次数',
  `process_start_time` int(10) NOT NULL DEFAULT '0' COMMENT '执行开始时间',
  `process_end_time` int(10) NOT NULL DEFAULT '0' COMMENT '执行结束时间',
  `process_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '执行状态 0 未执行 1 成功 2 失败',
  `process_msg` varchar(255) NOT NULL DEFAULT '' COMMENT '执行返回信息',
  PRIMARY KEY (`id`),
  KEY `idx_find_keyword` (`find_keyword`) USING BTREE,
  KEY `idx_msg_type` (`msg_type`) USING BTREE,
  KEY `idx_process_status` (`process_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='消息处理日志表';

# laravel_rabbitmq_demo

#### 介绍
laravel框架rabbitmq示例

#### 安装教程
1. 安装rabbitmq之后,运行install_look下的命令新建对应账号以及虚拟主机
2. 运行sql, 数据库默认设置  库名 demo_rabbitmq, 账号 root, 密码为空 

#### 使用说明
1. php artisan slow 监听slow队列
2. php artisan send 发送slow消息


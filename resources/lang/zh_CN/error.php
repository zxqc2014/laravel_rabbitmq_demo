<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 18:10
 */

use App\Service\ErrorCode;

return [
    ErrorCode::ERROR_SQL_COMMON => 'sql错误',
    ErrorCode::ERROR_STR_PARSE => '字符串解析错误',
    ErrorCode::ERROR_RABBIT_MQ => 'rabbitMQ错误',
];

<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 10:03
 */

namespace App\Model\Queue;


use App\Service\Traits\Singleton;
use App\Model\BaseModel;

/**
 * 消息处理错误日志表
 * Class MqProcessLog
 * @package App\Model\Queue
 * @author zxqc2018
 */
class MqProcessErrorLog extends BaseModel
{
    use Singleton;
    protected $table = 'mq_process_error_log';

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'msg_id',
        'create_time',
        'process_time',
    ];
}

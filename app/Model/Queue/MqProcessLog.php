<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 10:03
 */

namespace App\Model\Queue;


use App\Service\Traits\Singleton;
use App\Model\BaseModel;

/**
 * 消息处理日志表
 * Class MqProcessLog
 * @package App\Model\Queue
 * @author zxqc2018
 */
class MqProcessLog extends BaseModel
{
    use Singleton;
    protected $table = 'mq_process_log';

    public $timestamps = false;

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'msg_str',
        'msg_type',
        'find_keyword',
        'create_time',
        'process_num',
        'process_start_time',
        'process_end_time',
        'process_status',
        'process_msg',
    ];
}

<?php

use App\Service\ResultData;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

if (!function_exists('resultData')) {
    /**
    * 通用结果返回类
    * @param array $data 数据
    * @param int $code 错误码 0 默认正确
    * @param string $message 错误码描述
    * @param int $status_code http状态码
    * @return ResultData
    * @author zxqc2018
    */
    function resultData($data = [], $code = 0, $message = 'success', $status_code = 200): ResultData
    {
        return new ResultData($data, $code, $message, $status_code);
    }
}

if (!function_exists('exceptionMessageShow')) {

    /**
     * 异常消息本地环境增加调试信息输出
     * @param Throwable $e 异常对象
     * @param string $message 默认消息
     * @return string
     * @author zxqc2018
     */
    function exceptionMessageShow(Throwable $e, $message = '执行出现异常')
    {
        if (App::isLocal()) {
            Log::debug($e->getFile() . '|' .$e->getMessage() . '|' . $e->getLine());
        }

        return $message;
    }
}

if (!function_exists('myExplode')) {
    /**
     * 封装explode和join
     * @param mixed $data 处理的数据
     * @param string|array $separator 分割数据 字符串 explode 处理 数组 正则处理 如  ['[3-4\s]+']
     * @param bool $retArr 是否返回数组
     * @param null|int $arrLimit 返回数组并且原数据不是数组情况下有效[数组的长度]  默认不限制
     * @return array|string
     * @author zxqc2018
     */
    function myExplode($data, $separator = ',', $retArr = true, $arrLimit = null)
    {
        if (is_null($data)) {
            $data = [];
        }

        if ($retArr && is_array($data)) {
            return $data;
        }

        if (!$retArr && !is_array($data)) {
            return $data;
        }
        if (!is_array($data)) {
            //数组则当正则处理
            if (is_array($separator)) {
                $separator = $separator[0];
                $data      = preg_split('#' . preg_quote($separator, '/') . '#', $data, $arrLimit);
            } else {
                //由于默认传null  explode 会当成 1处理 所以
                if (is_null($arrLimit)) {
                    $data = explode($separator, $data);
                } else {
                    $data = explode($separator, $data, $arrLimit);
                }
            }
        }

        return $retArr ? $data : join($separator, $data);
    }
}

if (!function_exists('mixedTwoWayOpt')) {

    /**
     * 混合双向操作方法
     * @param mixed $data 处理的数据
     * @param int $optType 操作类型 1 序列号 2 json
     * @param bool $isForward 是否正操作
     * @param bool $isGrace 是否优雅处理[捕获异常]
     * @param array $extraData 额外数据
     * @return mixed
     * @author zxqc2018
     */
    function mixedTwoWayOpt($data, $optType = 1, $isForward = false, $isGrace = true, $extraData = [])
    {
        $defaultVal = $extraData['defaultVal'] ?? [];
        $res        = $data;

        try {
            switch ($optType) {
                case 1:
                    if (!$isForward) {
                        //替换转义后的双引号
                        $data = str_replace("&quot;", '"', $data);
                        $res  = unserialize($data);
                    } else {
                        $res = serialize($data);
                    }
                    break;
                case 2:
                    if (!$isForward) {
                        $res = json_decode($data, true);
                    } else {
                        $res = json_encode($data, JSON_UNESCAPED_UNICODE);
                    }
                    break;
            }
        } catch (Throwable $e) {
            if ($isGrace) {
                $res = $defaultVal;
            } else {
                \resultData([], \App\Service\ErrorCode::ERROR_STR_PARSE)->withException();
            }
        }

        //返回默认值
        if ($isGrace && is_null($res)) {
            $res = $defaultVal;
        }

        return $res;
    }
}

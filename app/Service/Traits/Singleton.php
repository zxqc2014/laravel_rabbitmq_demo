<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 15:57
 */

namespace App\Service\Traits;


/**
 * 单例trait
 * Trait Singleton
 * @package App\Service\Traits
 * @author zxqc2018
 */
trait Singleton
{

    private static $instance;

    /**
     * 获取静态实例
     * @param mixed ...$args 参数
     * @return Singleton
     * @author zxqc2018
     */
    public static function getInstance(...$args)
    {
        if (!isset(self::$instance)) {
            self::$instance = new static(...$args);
        }

        return self::$instance;
    }
}

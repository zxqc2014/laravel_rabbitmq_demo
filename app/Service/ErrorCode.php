<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 9:04
 */

namespace App\Service;

/**
 * 错误码
 * Class ErrorCode
 * @package App\Service
 */
class ErrorCode
{
    //sql错误
    const ERROR_SQL_COMMON = 10001;
    //解析错误
    const ERROR_STR_PARSE = 10002;
    //rabbitMq错误
    const ERROR_RABBIT_MQ = 10003;

    /**
     * 国际化
     * @param int $code
     * @return array|\Illuminate\Contracts\Translation\Translator|null|string
     * @author zxqc2018
     */
    public static function getMessage($code)
    {
        return trans("error.{$code}");
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 9:04
 */

namespace App\Service;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * 通用数据返回对象
 * Class ResultData
 * @package App\Service
 * @author zxqc2018
 */
class ResultData implements \ArrayAccess
{

    /**
     * 异常情况下message上附带Data数据的分隔符
     * @var string
     */
    const MSG_DATA_SEPARATOR = '@=@=@';
    /**
     * 错误码
     * @var int
     */
    private $code;

    /**
     * 错误码描述
     * @var string
     */
    private $message;

    /**
     * 返回数据
     * @var array
     */
    private $data;

    /**
     * http请求状态码 200 正常 417 异常
     * @var int
     */
    private $status_code;

    /**
     * ResultData constructor.
     * @param int $code
     * @param string $message
     * @param array $data
     * @param int $status_code
     */
    public function __construct(array $data = [], int $code = 0, string $message = 'success', int $status_code = 200)
    {
        $this->code = $code;
        $this->data = $data;

        //处理通用错误code对应错误码message情况
        if ($code > 0 ) {
            $tmp_msg = $message == 'success' ? '' : $message;

            if (!empty($tmp_msg)) {
                $error_msg = $tmp_msg;
            } else {
                $error_msg = ErrorCode::getMessage($code);
            }
            $message = $error_msg;

            $status_code = 417;
        }
        $this->status_code = $status_code;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->status_code;
    }

    /**
     * @param int $status_code
     */
    public function setStatusCode(int $status_code)
    {
        $this->status_code = $status_code;
    }

    /**
     * 合并data
     * @param array $data
     * @return array
     * @author zxqc2018
     */
    public function mergeData(array $data)
    {
        $this->data = array_merge($this->data, $data);
        return $data;
    }
    /**
     * 返回数组形式
     * @return array
     * @author zxqc2018
     */
    public function toArray()
    {
        $res = [
            'code' => $this->getCode(),
            'message' => $this->getMessage(),
            'data' => $this->getData(),
            'status_code' => $this->getStatusCode(),
        ];

        return $res;
    }

    /**
     * 默认输出json字符串
     * @return string
     * @author zxqc2018
     */
    public function __toString()
    {
        return json_encode($this->toArray());
    }

    /**
     * 根据需要抛出异常
     * @param array $exclude_code_arr 排除抛出异常的code数组
     * @return $this
     * @author zxqc2018
     */
    public function withException($exclude_code_arr = [])
    {

        //处理需要抛出异常的情况
        if ($this->getCode() > 0 && $this->getStatusCode() == 417 && !in_array($this->getCode(), $exclude_code_arr)) {
            //拼接异常情况下data数据到message
            if (!empty($this->getData())) {
                $this->setMessage($this->getMessage() . static::MSG_DATA_SEPARATOR . mixedTwoWayOpt($this->getData(), 2, true));
            }
            throw new HttpException($this->getStatusCode(), $this->getMessage(), null, [], $this->getCode());
        }
        return $this;
    }

    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }
}

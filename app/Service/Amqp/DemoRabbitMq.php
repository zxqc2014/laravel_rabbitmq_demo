<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 16:34
 */

namespace App\Service\Amqp;


use App\Service\ErrorCode;
use App\Service\Traits\Singleton;

/**
 * demo-rabbitMq类
 * Class DemoRabbitMq
 * @package App\Service\Amqp
 * @author zxqc2018
 */
class DemoRabbitMq extends AbstractRabbitMq
{
    use Singleton;
    //mq配置
    protected $configName = 'demo';
    //默认交换机
    protected $defaultExchangeName = 'demo-exchange';
    //快中慢-队列配置
    protected $queuePriorityConfig = [
        'fast' => ['demo-fast-queue', 'demo.fast.#'],
        'middle' => ['demo-middle-queue', 'demo.middle.#'],
        'slow' => ['demo-slow-queue', 'demo.slow.#'],
    ];

    /**
     * 设置routeKey对应处理方法
     * @author zxqc2018
     */
    function settingRouteKeyProcessFunc()
    {
        $this->routeKeyProcessFunc[self::DEMO_FAST_TEST] = function ($msgData) {
            print_r($msgData);
            $res = \resultData();
            $res->setMessage('fast');
            return $res;
        };
        $this->routeKeyProcessFunc[self::DEMO_SLOW_TEST] = function ($msgData) {
            print_r($msgData);
            if (rand(1,10) > 5) {
                return resultData([], ErrorCode::ERROR_RABBIT_MQ, '测试处理失败');
            }
            $res = \resultData();
            $res->setMessage('slow');
            return $res;
        };
    }
}

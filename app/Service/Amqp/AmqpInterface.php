<?php
/**
 * Created by PhpStorm.
 * User: zxqc2018
 * Date: 2018/12/24
 * Time: 16:48
 */

namespace App\Service\Amqp;

use App\Service\ResultData;

/**
 * 队列接口
 * Class AmqpInterface
 * @package App\Service\Amqp
 * @author zxqc2018
 */
interface AmqpInterface
{
    /**
     * 队列监听处理方法
     * @param string $queueName 队列名
     * @param array|callable $processFunc 执行方法   匿名函数|[对象,对象方法]
     * @return mixed
     * @author zxqc2018
     */
    function msgListenerProcess($queueName, $processFunc);

    /**
     * 发送消息
     * @param array $data 消息内容
     * @param array|string $exchangeParam 交换机配置 [名字, 交换机类型 direct|fanout|topic]
     * @param array|string $queueParam 队列配置 |[名字, 队列绑定的路由键]
     * @param string $routeKey 消息的路由键
     * @return ResultData
     * @author zxqc2018
     */
    function sendMsg($data, $exchangeParam, $queueParam, $routeKey);

    /**
     * 发送优先级定义消息
     * @param array $data 消息数据
     * @param string $routeKey 路由key
     * @return mixed
     * @author zxqc2018
     */
    function sendPriorityMsg($data, $routeKey);

    /**
     * 重新发送优先级定义消息
     * @param string $routeKey 路由key
     * @param int $msgId 消息ID
     * @return mixed
     * @author zxqc2018
     */
    function resendPriorityMsg($routeKey, $msgId);

    /**
     * 处理消息方法
     * @param array msgData mq_process_log 行数据
     * @return mixed
     * @author zxqc2018
     */
    function msgProcess(array $msgData);

    /**
     * 设置routeKey对应处理方法
     * @author zxqc2018
     */
    function settingRouteKeyProcessFunc();
}

<?php

namespace App\Console\Commands;

use App\Service\Amqp\DemoRabbitMq;
use Illuminate\Console\Command;

class Resend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resend {routeKey} {msgId?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '从发单条';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $arguments = $this->arguments();
        $routeKey = $arguments['routeKey'] ?? 0;
        $msgId = $arguments['msgId'] ?? 0;
        DemoRabbitMq::getInstance()->resendPriorityMsg($routeKey, $msgId);
    }
}

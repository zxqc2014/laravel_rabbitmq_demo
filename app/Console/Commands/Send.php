<?php

namespace App\Console\Commands;

use App\Service\Amqp\DemoRabbitMq;
use Illuminate\Console\Command;

class Send extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '测试发送';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DemoRabbitMq::getInstance()->sendPriorityMsg(DemoRabbitMq::wrapFindKeyword(['a' => rand(1,1000)], 1), DemoRabbitMq::DEMO_SLOW_TEST);
    }
}

<?php

namespace App\Console\Commands;

use App\Service\Amqp\DemoRabbitMq;
use Illuminate\Console\Command;

class ListenerSlow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '消息slow队列监听';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DemoRabbitMq::getInstance()->listenerProcess(DemoRabbitMq::QUEUE_PRIORITY_SLOW_KEY);
    }
}
